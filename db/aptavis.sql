-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.30 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table aptavis.tbl_data_klub
CREATE TABLE IF NOT EXISTS `tbl_data_klub` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama_klub` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `kota` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `create_at` datetime NOT NULL,
  `create_by` varchar(200) NOT NULL,
  `update_at` datetime NOT NULL,
  `update_by` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table aptavis.tbl_data_klub: ~4 rows (approximately)
DELETE FROM `tbl_data_klub`;
INSERT INTO `tbl_data_klub` (`id`, `nama_klub`, `kota`, `is_active`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES
	(1, 'Persib', 'Bandung', 1, '2023-10-24 22:31:03', 'Ibeffaro', '0000-00-00 00:00:00', ''),
	(2, 'Arema', 'Malang', 1, '2023-10-24 22:31:48', 'Ibeffaro', '0000-00-00 00:00:00', ''),
	(3, 'Persija', 'Jakarta', 1, '2023-10-24 22:32:01', 'Ibeffaro', '0000-00-00 00:00:00', ''),
	(4, 'Bali United', 'Bali', 1, '2023-10-25 19:23:59', 'Ibeffaro', '0000-00-00 00:00:00', '');

-- Dumping structure for table aptavis.tbl_data_skor
CREATE TABLE IF NOT EXISTS `tbl_data_skor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_klub1` int NOT NULL DEFAULT '0',
  `klub1` varchar(50) NOT NULL DEFAULT '0',
  `skor1` int NOT NULL DEFAULT '0',
  `id_klub2` int NOT NULL,
  `klub2` varchar(50) NOT NULL DEFAULT '',
  `skor2` int NOT NULL DEFAULT '0',
  `create_at` datetime NOT NULL,
  `create_by` varchar(100) NOT NULL,
  `update_at` datetime NOT NULL,
  `update_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table aptavis.tbl_data_skor: ~4 rows (approximately)
DELETE FROM `tbl_data_skor`;
INSERT INTO `tbl_data_skor` (`id`, `id_klub1`, `klub1`, `skor1`, `id_klub2`, `klub2`, `skor2`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES
	(1, 1, 'Persib', 2, 3, 'Persija', 0, '2023-10-25 14:57:04', 'Ibeffaro', '2023-10-25 15:13:17', 'Ibeffaro'),
	(2, 1, 'Persib', 2, 2, 'Arema', 0, '2023-10-25 15:22:05', 'Ibeffaro', '0000-00-00 00:00:00', ''),
	(3, 2, 'Arema', 2, 3, 'Persija', 0, '2023-10-25 15:54:56', 'Ibeffaro', '0000-00-00 00:00:00', ''),
	(4, 1, 'Persib', 1, 4, 'Bali United', 0, '2023-10-25 19:24:34', 'Ibeffaro', '0000-00-00 00:00:00', '');

-- Dumping structure for table aptavis.tbl_history_password
CREATE TABLE IF NOT EXISTS `tbl_history_password` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_user` int NOT NULL,
  `password` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `tanggal` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table aptavis.tbl_history_password: 1 rows
DELETE FROM `tbl_history_password`;
/*!40000 ALTER TABLE `tbl_history_password` DISABLE KEYS */;
INSERT INTO `tbl_history_password` (`id`, `id_user`, `password`, `tanggal`) VALUES
	(1, 2, '17c4520f6cfd1ab53d8745e84681eb49', '2023-10-24 22:12:59');
/*!40000 ALTER TABLE `tbl_history_password` ENABLE KEYS */;

-- Dumping structure for table aptavis.tbl_kode
CREATE TABLE IF NOT EXISTS `tbl_kode` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tabel` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `kolom` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `panjang` int NOT NULL,
  `awalan` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `akhiran` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `create_at` datetime NOT NULL,
  `create_by` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `update_at` datetime NOT NULL,
  `update_by` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table aptavis.tbl_kode: 1 rows
DELETE FROM `tbl_kode`;
/*!40000 ALTER TABLE `tbl_kode` DISABLE KEYS */;
INSERT INTO `tbl_kode` (`id`, `tabel`, `kolom`, `panjang`, `awalan`, `akhiran`, `is_active`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES
	(1, 'tbl_user', 'kode', 5, '{field_kategori_user}{y}{m}', '', 1, '2023-06-05 21:54:10', 'Ibeffaro', '0000-00-00 00:00:00', '');
/*!40000 ALTER TABLE `tbl_kode` ENABLE KEYS */;

-- Dumping structure for table aptavis.tbl_menu
CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `id` int NOT NULL AUTO_INCREMENT,
  `parent_id` int NOT NULL,
  `level1` int NOT NULL,
  `level2` int NOT NULL,
  `level3` int NOT NULL,
  `level4` int NOT NULL,
  `nama` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `target` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `icon` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `akses_view` tinyint(1) NOT NULL DEFAULT '1',
  `akses_input` tinyint(1) NOT NULL,
  `akses_edit` tinyint(1) NOT NULL,
  `akses_delete` tinyint(1) NOT NULL,
  `akses_additional` tinyint(1) NOT NULL,
  `alias_input` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `alias_edit` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `alias_delete` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `alias_additional` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `urutan` int NOT NULL,
  `shortcut` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `create_by` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL,
  `update_by` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table aptavis.tbl_menu: 19 rows
DELETE FROM `tbl_menu`;
/*!40000 ALTER TABLE `tbl_menu` DISABLE KEYS */;
INSERT INTO `tbl_menu` (`id`, `parent_id`, `level1`, `level2`, `level3`, `level4`, `nama`, `target`, `icon`, `akses_view`, `akses_input`, `akses_edit`, `akses_delete`, `akses_additional`, `alias_input`, `alias_edit`, `alias_delete`, `alias_additional`, `urutan`, `shortcut`, `is_active`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
	(1, 0, 1, 0, 0, 0, 'Pengaturan', 'settings', 'fa-cog', 1, 0, 0, 0, 0, '', '', '', '', 4, '', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(2, 1, 1, 2, 0, 0, 'Aplikasi', 'application', 'fa-drafting-compass', 1, 0, 0, 0, 0, '', '', '', '', 1, '', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(3, 2, 1, 2, 3, 0, 'Menu', 'menu', 'fa-th-large', 1, 1, 1, 1, 0, '', '', '', '', 1, 'Alt+M', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(4, 2, 1, 2, 4, 0, 'Pengaturan Website', 'web_setting', 'fa-globe', 1, 0, 0, 0, 0, '', '', '', '', 2, 'Alt+Shift+W', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(5, 0, 5, 0, 0, 0, 'Manajemen Pengguna', 'manajemen_pengguna', 'fa-user', 1, 0, 0, 0, 0, '', '', '', '', 3, '', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(6, 5, 5, 6, 0, 0, 'Daftar Pengguna', 'user_lists', 'fa-user', 1, 1, 1, 1, 0, '', '', '', '', 1, 'Alt+U', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(7, 5, 5, 7, 0, 0, 'Hak Akses Pengguna', 'user_roles', 'fa-user-cog', 1, 1, 1, 1, 0, '', '', '', '', 2, 'Alt+R', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(8, 2, 1, 2, 8, 0, 'Kode Otomatis', 'auto_code', 'fa-laptop-code', 1, 1, 1, 1, 0, '', '', '', '', 3, 'Alt+C', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(9, 1, 1, 9, 0, 0, 'Pengumuman', 'pengumuman', 'fa-bullhorn', 1, 1, 1, 1, 0, '', '', '', '', 2, '', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(10, 2, 1, 2, 10, 0, 'Bahasa', 'bahasa', 'fa-language', 1, 1, 1, 1, 0, '', '', '', '', 4, 'Alt+L', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(11, 1, 1, 11, 0, 0, 'Backup dan Restore', 'backup_restore', 'fa-hdd', 1, 0, 0, 0, 0, '', '', '', '', 3, '', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(12, 11, 1, 11, 12, 0, 'Backup', 'backup', 'fa-save', 1, 1, 0, 1, 1, '', '', '', 'Unduh', 1, '', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(13, 11, 1, 11, 13, 0, 'Restore', 'restore', 'fa-redo', 1, 0, 0, 0, 1, '', '', '', 'Unggah', 2, '', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(14, 1, 1, 14, 0, 0, 'Log', 'log', 'fa-history', 1, 0, 0, 0, 0, '', '', '', '', 4, '', 1, 'Ibeffaro', '2023-06-05 21:54:10', '', '0000-00-00 00:00:00'),
	(15, 0, 15, 0, 0, 0, 'Master Data', 'master_data', 'fa-database', 1, 0, 0, 0, 0, '', '', '', '', 2, '', 0, 'Ibeffaro', '2023-08-05 13:16:41', 'Ibeffaro', '2023-10-24 22:19:22'),
	(16, 17, 17, 16, 0, 0, 'Klasemen', 'klasemen', 'fa-file-chart-line', 1, 0, 0, 0, 0, '', '', '', '', 3, '', 1, 'Ibeffaro', '2023-08-05 13:17:14', 'Ibeffaro', '2023-10-25 15:58:52'),
	(17, 0, 17, 0, 0, 0, 'Modul', 'modul', 'fa-modx', 1, 0, 0, 0, 0, '', '', '', '', 1, '', 1, 'Ibeffaro', '2023-08-05 13:18:21', 'Ibeffaro', '2023-08-05 13:40:25'),
	(18, 17, 17, 18, 0, 0, 'Data Klub', 'klub', 'fa-volleyball-ball', 1, 1, 1, 1, 0, '', '', '', '', 1, '', 1, 'Ibeffaro', '2023-08-05 13:18:59', 'Ibeffaro', '2023-10-24 22:22:42'),
	(19, 17, 17, 19, 0, 0, 'Data Skor', 'skor', 'fa-sort-numeric-up', 1, 1, 1, 1, 0, '', '', '', '', 2, '', 1, 'Ibeffaro', '2023-08-05 13:20:14', 'Ibeffaro', '2023-10-24 22:22:33');
/*!40000 ALTER TABLE `tbl_menu` ENABLE KEYS */;

-- Dumping structure for table aptavis.tbl_notifikasi
CREATE TABLE IF NOT EXISTS `tbl_notifikasi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `notif_link` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `notif_date` datetime NOT NULL,
  `notif_type` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `notif_icon` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'fi-bell',
  `is_read` tinyint(1) NOT NULL,
  `id_user` int NOT NULL,
  `transaksi` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `id_transaksi` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table aptavis.tbl_notifikasi: 0 rows
DELETE FROM `tbl_notifikasi`;
/*!40000 ALTER TABLE `tbl_notifikasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_notifikasi` ENABLE KEYS */;

-- Dumping structure for table aptavis.tbl_pengumuman
CREATE TABLE IF NOT EXISTS `tbl_pengumuman` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_user` int NOT NULL,
  `pengumuman` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `tanggal_publish` datetime NOT NULL,
  `tanggal_selesai` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `create_at` datetime NOT NULL,
  `create_by` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `update_at` datetime NOT NULL,
  `update_by` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table aptavis.tbl_pengumuman: 0 rows
DELETE FROM `tbl_pengumuman`;
/*!40000 ALTER TABLE `tbl_pengumuman` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_pengumuman` ENABLE KEYS */;

-- Dumping structure for table aptavis.tbl_setting
CREATE TABLE IF NOT EXISTS `tbl_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `_key` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_value` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `serialize` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table aptavis.tbl_setting: ~94 rows (approximately)
DELETE FROM `tbl_setting`;
INSERT INTO `tbl_setting` (`id`, `_key`, `_value`, `serialize`) VALUES
	(1, 'title', 'Aptavis', 0),
	(2, 'deskripsi', '', 0),
	(3, 'logo', '6fa963e3e1351055d9a4704823c6af59.png', 0),
	(4, 'favicon', 'c74bea53b1880f46b05ae4d4de283b9e.png', 0),
	(5, 'single_login', '0', 0),
	(6, 'sess_exp', '30', 0),
	(7, 'tipe_menu', 'sidebar', 0),
	(8, 'company', 'PT. Digital Apta Media', 0),
	(9, 'logo_true_color', '0', 0),
	(10, 'smtp_server', 'tls://smtp.gmail.com', 0),
	(11, 'smtp_port', '465', 0),
	(12, 'smtp_email', 'ibeffaro@gmail.com', 0),
	(13, 'smtp_password', '', 0),
	(14, 'custom_template', '1', 0),
	(15, 'warna_primary', '#274471', 0),
	(16, 'warna_secondary', '#06375a', 0),
	(17, 'warna_border', '#0b5394', 0),
	(18, 'warna_dropdown', 'd-white', 0),
	(19, 'warna_primary_hover', '#16537e', 0),
	(20, 'warna_text_header', '#f3f2f5', 0),
	(21, 'ukuran_tampilan', 'small', 0),
	(22, 'sensor_data', '0', 0),
	(23, 'realtime_notification', '0', 0),
	(24, 'email_notification', '0', 0),
	(25, 'alamat_perusahaan', 'Jl. Mustang Komplek Kumala Garden B5-6 Bandung, Jawa Barat 40164', 0),
	(26, 'periode_transaksi', 'bulan_berjalan', 0),
	(27, 'logo_perusahaan', 'bd516b2de1bbd683534e97fa07be5f27.png', 0),
	(28, 'bg_btn_primary', '#00b5b8', 0),
	(29, 'border_btn_primary', '#00a5a8', 0),
	(30, 'color_btn_primary', 'white', 0),
	(31, 'hover_bg_btn_primary', '#26c0c3', 0),
	(32, 'hover_border_btn_primary', '#00aeb1', 0),
	(33, 'hover_color_btn_primary', 'white', 0),
	(34, 'focus_bg_btn_primary', '#009da0', 0),
	(35, 'focus_border_btn_primary', '#00a5a8', 0),
	(36, 'focus_color_btn_primary', 'white', 0),
	(37, 'bg_btn_info', '#2dcee3', 0),
	(38, 'border_btn_info', '#22c2dc', 0),
	(39, 'color_btn_info', 'white', 0),
	(40, 'hover_bg_btn_info', '#4dd5e7', 0),
	(41, 'hover_border_btn_info', '#28c9e0', 0),
	(42, 'hover_color_btn_info', 'white', 0),
	(43, 'focus_bg_btn_info', '#1cbcd8', 0),
	(44, 'focus_border_btn_info', '#22c2dc', 0),
	(45, 'focus_color_btn_info', 'white', 0),
	(46, 'bg_btn_success', '#16d39a', 0),
	(47, 'border_btn_success', '#10c888', 0),
	(48, 'color_btn_success', 'white', 0),
	(49, 'hover_bg_btn_success', '#39daa9', 0),
	(50, 'hover_border_btn_success', '#13ce92', 0),
	(51, 'hover_color_btn_success', 'white', 0),
	(52, 'focus_bg_btn_success', '#0cc27e', 0),
	(53, 'focus_border_btn_success', '#10c888', 0),
	(54, 'focus_color_btn_success', 'white', 0),
	(55, 'bg_btn_warning', '#ffa87d', 0),
	(56, 'border_btn_warning', '#ff976a', 0),
	(57, 'color_btn_warning', 'white', 0),
	(58, 'hover_bg_btn_warning', '#ffb591', 0),
	(59, 'hover_border_btn_warning', '#ffa075', 0),
	(60, 'hover_color_btn_warning', 'white', 0),
	(61, 'focus_bg_btn_warning', '#ff8d60', 0),
	(62, 'focus_border_btn_warning', '#ff976a', 0),
	(63, 'focus_color_btn_warning', 'white', 0),
	(64, 'bg_btn_danger', '#ff7588', 0),
	(65, 'border_btn_danger', '#ff6275', 0),
	(66, 'color_btn_danger', 'white', 0),
	(67, 'hover_bg_btn_danger', '#ff8a9a', 0),
	(68, 'hover_border_btn_danger', '#ff6d80', 0),
	(69, 'hover_color_btn_danger', 'white', 0),
	(70, 'focus_bg_btn_danger', '#ff586b', 0),
	(71, 'focus_border_btn_danger', '#ff6275', 0),
	(72, 'focus_color_btn_danger', 'white', 0),
	(73, 'query', '0', 0),
	(74, 'onesignal_app_id', 'c055cd23-264c-4375-8f7c-8abe52904f5d', 0),
	(75, 'onesignal_api_key', 'NmU4YzQzYjMtNzFiYS00NWVjLTliMDMtODQ4N2U1NDY2MWE0', 0),
	(76, 'kebijakan', '&lt;p&gt;&lt;strong&gt;Penggunaan Website eproc.pegadaian.co.id&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;PT Pegadaian (persero) berkomitmen untuk menjaga privasi Pengguna. Pernyataan Privasi ini menerangkan langkah-langkah untuk menjaga data pribadi yang masuk pada Website Pegadaian . Pernyataan Privasi ini menerangkan kategori data pribadi yang dikumpulkan, tujuan penggunaan data yang masuk, pengamanan data, dan pembaharuan data (misalnya alamat, nomor telepon, email). Dengan mengakses Website Pegadaian, Pengguna menyetujui proses pengumpulan data dan praktik penggunaanya seperti yang dijelaskan dalam Pernyataan Privasi ini. Pegadaian berhak untuk memverifikasi identitas Pengguna sebelum memberikan akses ke data pribadi Pengguna.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Pengumpulan DataÂ &lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;Pengguna dapat mengunjungi Website Pegadaian tanpa harus memberikan data pribadi. Akan tetapi, pada halaman Karir, Pegadaian dapat menanyakan data pribadi Pengguna agar dapat memberikan layanan kepada Pengguna.&lt;/p&gt;\r\n&lt;p&gt;Data Pengguna dikumpulkan secara otomatis, termasuk halaman yang dikunjungi, jumlah bit yang ditransfer, tautan yang diklik, dan tindakan lain yang diambil dalam Website Pegadaian. Pada Website Pegadaian di mana Pengguna melakukan login, Pegadaian dapat menghubungkan informasi ini dengan identitas Pengguna. Pegadaian juga dapat menggunakan informasi ini untuk meningkatkan langkah-langkah keamanan Pegadaian. Pegadaian juga mengumpulkan informasi tertentu yang dikirim oleh browser Pengguna kepada setiap situs elektronik yang dikunjungi, seperti alamat protokol internet, jenis, kemampuan dan bahasa browser Pengguna, sistem operasi Pengguna, tanggal dan waktu Pengguna mengakses atau menggunakan Website Pegadaian, dan situs elektronik yang digunakan oleh Pengguna untuk menjelajah Website Pegadaian.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Penggunaan Data Pribadi&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;Pegadaian hanya akan menggunakan data pribadi Pengguna untuk pertimbangan perekrutan karyawan perseroan dan anak-anak perusahaan. Pegadaian mungkin menyimpan data dan informasi tersebut untuk keperluan di masa mendatang.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Penggunaan CookiesÂ &lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;Cookies adalah file-file kecil yang disimpan situs elektronik ke dalam piranti keras Pengguna atau ke memori browser Pengguna. Website Pegadaian dapat menggunakannya untuk melacak berapa kali Pengguna mengunjungi Website Pegadaian, melacak jumlah pengunjung ke Website Pegadaian, untuk menentukan dan menganalisis penggunaan Website Pegadaian, untuk menyimpan data yang Pengguna berikan (seperti preferensi), dan untuk menyimpan informasi teknis terkait interaksi Pengguna dengan Website Pegadaian.&lt;/p&gt;\r\n&lt;p&gt;Sebagian besar browser internet menerima cookies secara otomatis, tetapi Pengguna dapat memodifikasi pengaturan browser Pengguna untuk menolak cookies atau untuk memberitahu Pengguna jika cookies dipasang/tersedia pada komputer Pengguna. Jika Pengguna memilih untuk tidak menerima cookies, Pengguna mungkin tidak akan dapat membuka semua informasi yang terdapat dalam Website Pegadaian Browser internet juga memungkinkan Pengguna untuk menghapus cookies yang ada, walaupun ini berarti bahwa pengaturan yang ada (termasuk user ID dan preferensi tersimpan lainnya) akan terhapus.&lt;/p&gt;\r\n&lt;p&gt;Website Pegadaian juga dapat berisikan gambar-gambar elektronik yang dikenal sebagai web beacons atau single-pixel gifs, yang memungkinkan Pegadaian menghitung jumlah pengunjung yang telah mengunjungi halaman-halaman tersebut.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Keamanan&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;Pegadaian berkomitmen untuk menjaga keamanan data pribadi. Walaupun tidak ada langkah pengamanan yang menjamin keamanan data sepenuhnya, Pegadaian berusaha menggunakan berbagai teknologi dan prosedur pengamanan yang dianggap relevan untuk membantu menjaga data dari akses, penggunaan, atau pengungkapan yang melanggar hukum. Data pribadi yang pernah masuk tidak akan diungkapkan kepada pihak di luar Pegadaian tanpa izin Pengguna, kecuali tindakan tersebut diperlukan untuk memenuhi ketentuan peraturan perundang-undangan, untuk menjaga dan mempertahankan hak-hak Pegadaian, atau dalam keadaan mendesak untuk melindungi keselamatan diri seseorang.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Perubahan Pernyataan Privasi&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;Pegadaian dapat secara berkala memperbarui Pernyataan Privasi ini, dan Pengguna dianggap menyetujui segala perubahan terkait Pernyataan Privasi ini.&lt;/p&gt;\r\n', 0),
	(77, 'disclaimer', '&lt;p style=&quot;text-align:center&quot;&gt;&lt;strong&gt;PENDAFTARAN ONLINE REKANAN E-PROCUREMENT&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Setiap Penyedia Barang dan Jasa pengguna aplikasi E-Procurement PT. Pegadaian (persero), diatur oleh ketentuan sebagai berikut.&lt;/p&gt;\r\n\r\n&lt;p&gt;I. KETENTUAN UMUM&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;Sistem Online Procuement adalah perangkat lunak aplikasi yang bertujuan memfasilitasi Rekanan agar dapat melakukan transaksi pengadaan barang dan jasa melalui media internet.&lt;/li&gt;\r\n	&lt;li&gt;Rekanan wajib tunduk pada persyaratan yang tertera dalam ketentuan ini serta kebijakan lain yang ditetapkan oleh PT. Pegadaian (persero) sebagai pengelola situs https://eproc.pegadaian.co.id/&lt;/li&gt;\r\n	&lt;li&gt;Transaksi melalui Sistem Online Procurement hanya boleh dilakukan/diikuti oleh Rekanan yang sudah terdaftar dan teraktivasi untuk bisa mengikuti transaksi secara elektronik.&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p&gt;II. PERSYARATAN KEANGGOTAAN E-PROCUREMENT PT. PEGADAIAN (PERSERO)&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;Rekanan harus berbentuk badan usaha atau perseorangan dan dianggap mampu melakukan perbuatan hukum.&lt;/li&gt;\r\n	&lt;li&gt;Untuk mendapatkan akun dalam Sistem Online Procurement, calon Rekanan terlebih dahulu harus melakukan registrasi online dengan data yang benar dan akurat, sesuai dengan keadaan yang sebenarnya.&lt;/li&gt;\r\n	&lt;li&gt;Calon Rekanan dapat melakukan transaksi melalui Sistem Online Procurement, apabila telah menerima konfirmasi aktivasi keanggotaannya dari Sistem Online Procurement.&lt;/li&gt;\r\n	&lt;li&gt;Rekanan wajib memperbaharui data perusahaannya jika tidak sesuai lagi dengan keadaan yang sebenarnya atau jika tidak sesuai dengan ketentuan ini.&lt;/li&gt;\r\n	&lt;li&gt;Akun dalam Sistem Online Procurement akan berakhir apabila:\r\n	&lt;ul&gt;\r\n		&lt;li&gt;Rekanan mengundurkan diri dengan cara mengirimkan email atau surat kepada PT. Pegadaian (persero) sebagai pengelola situs http://eproc.pegadaian.co.id/ dan mendapatkan email atau surat konfirmasi atas pengunduran dirinya.&lt;/li&gt;\r\n		&lt;li&gt;Melanggar ketentuan yang telah ditetapkan oleh PT. Pegadaian (persero) sebagai pengelola situs https://eproc.pegadaian.co.id/ dan mendapatkan email atau surat konfirmasi atas pengunduran dirinya.&lt;/li&gt;\r\n	&lt;/ul&gt;\r\n	&lt;/li&gt;\r\n	&lt;li&gt;Rekanan setuju bahwa transaksi melalui Sistem Online Procurement tidak boleh menyalahi peraturan perundangan maupun etika bisnis yang berlaku di Indonesia.&lt;/li&gt;\r\n	&lt;li&gt;Rekanan tunduk pada semua peraturan yang berlaku di Indonesia yang berhubungan dengan, tetapi tidak terbatas pada, penggunaan jaringan yang terhubung pada jasa dan transmisi data teknis, baik di wilayah Indonesia maupun ke luar dari wilayah Indonesia melalui Sistem Online Procurement yang sesuai Undang-Undang Republik Indonesia Nomor 11, Tahun 2008, Tentang Informasi dan Transaksi Elektronik (UU ITE).&lt;/li&gt;\r\n	&lt;li&gt;Rekanan menyadari bahwa usaha apapun untuk dapat menembus sistem komputer dengan tujuan memanipulasi Sistem Online Procurement merupakan tindakan melanggar hukum.&lt;/li&gt;\r\n	&lt;li&gt;PT. Pegadaian (persero) sebagai pengelola situs https://eproc.pegadaian.co.id/ berhak memutuskan perjanjian dengan Rekanan secara sepihak apabila Rekanan dianggap tidak dapat menaati ketentuan yang ada.&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p&gt;III. TANGGUNG JAWAB PENYEDIA BARANG DAN JASA&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;Rekanan bertanggung jawab atas penjagaan kerahasiaan passwordnya dan bertanggung jawab atas transaksi dan kegiatan lain yang menggunakan akun miliknya.&lt;/li&gt;\r\n	&lt;li&gt;Rekanan setuju untuk segera memberitahukan kepada PT. Pegadaian (persero) sebagai pengelola situs http://eproc.pegadaian.co.id/ apabila mengetahui adanya penyalahgunaan akun miliknya oleh pihak lain yang tidak berhak dan/atau jika ada gangguan keamanan atas akun miliknya itu.&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p&gt;IV. PERUBAHAN KETENTUAN&lt;/p&gt;\r\n\r\n&lt;p&gt;PT. Pegadaian (persero) sebagai pengelola situs http://eproc.pegadaian.co.id/ dapat memperbaiki, menambah, atau mengurangi ketentuan ini setiap saat, dengan atau tanpa pemberitahuan sebelumnya. Setiap Rekanan terikat dan tunduk kepada ketentuan yang telah diperbaiki/ditambah/dikurangi.&lt;/p&gt;\r\n', 0),
	(78, 'alias_email', 'support@ibef.site', 0),
	(79, 'nama_alias_email', 'Ibef Faro', 0),
	(80, 'telp_perusahaan', '022-2017293', 0),
	(81, 'faks_perusahaan', '', 0),
	(82, 'chatting', '0', 0),
	(83, 'ws_server', 'ws://192.168.43.226:8080', 0),
	(84, 'warna_notifikasi', '#ff5050', 0),
	(85, 'masa_aktif_password', '', 0),
	(86, 'jumlah_history_password', '2', 0),
	(87, 'jumlah_salah_password', '5', 0),
	(88, 'log_aktif', '0', 0),
	(89, 'fa_icon', 'fontawesome.regular', 0),
	(90, 'fileupload_mimes', 'jpg,jpeg,png,bmp,gif,pdf,xpdf,zip,xls,xlsx,webp', 0),
	(91, 'email_perusahaan', '', 0),
	(92, 'icon_perusahaan', '0412be440f618a4f7c956dff37c3d83b.png', 0),
	(96, 'role_dev', '1', 0),
	(97, 'role_superadmin', '2', 0);

-- Dumping structure for table aptavis.tbl_user
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_group` int NOT NULL,
  `id_karyawan` int NOT NULL,
  `kode` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `nama` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `telepon` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `foto` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `alamat` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `kategori_user` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'U',
  `is_active` tinyint(1) NOT NULL,
  `is_block` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_login` tinyint(1) NOT NULL,
  `last_activity` datetime NOT NULL,
  `token_app` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `notification_id` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `ip_address` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `invalid_password` tinyint NOT NULL,
  `create_by` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL,
  `update_by` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `update_at` datetime NOT NULL,
  `change_password_by` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `change_password_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table aptavis.tbl_user: 2 rows
DELETE FROM `tbl_user`;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`id`, `id_group`, `id_karyawan`, `kode`, `nama`, `username`, `password`, `email`, `telepon`, `foto`, `jenis_kelamin`, `alamat`, `kategori_user`, `is_active`, `is_block`, `last_login`, `is_login`, `last_activity`, `token_app`, `notification_id`, `ip_address`, `invalid_password`, `create_by`, `create_at`, `update_by`, `update_at`, `change_password_by`, `change_password_at`) VALUES
	(1, 1, 0, '0000000', 'Ibeffaro', 'ibeffaro', '$2y$09$4.Ijpwkp3ycd1BgidGZjmOqMphz4J1Xa5Hmv5hgKch1gPMGleUxAW', 'ibeffaro@gmail.com', '6285899991774', '80a58a834ae8335c00df2770ada4d2d4.jpg', '', '', 'U', 1, 0, '2023-10-25 23:17:33', 1, '2023-10-25 23:17:47', 'raopvyRHYpKBDPiYwyXkm', '', '127.0.0.1', 0, 'Ibeffaro', '2023-06-05 21:48:10', 'Ibeffaro', '2023-08-02 21:54:19', 'Ibeffaro', '2023-08-02 21:54:19'),
	(2, 2, 0, 'U230600001', 'Super Admin', 'superadmin', '$2y$09$KQ0/fOCYVADa2aWq5fLi1.nXzmZUsZhhnrcvRjew3Li32VyrpP5Sm', 'superadmin@gmail.com', '6289796262700', '94045b8c8b70f3d9440c4708897212a0.png', '', '', 'U', 1, 0, '2023-10-25 23:05:13', 0, '2023-10-25 23:09:35', '', '', '127.0.0.1', 0, 'Ibeffaro', '2023-06-06 22:03:31', 'Ibeffaro', '2023-10-24 22:12:59', 'Ibeffaro', '2023-10-24 22:12:59');
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;

-- Dumping structure for table aptavis.tbl_user_akses
CREATE TABLE IF NOT EXISTS `tbl_user_akses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_group` int NOT NULL,
  `id_menu` int NOT NULL,
  `act_view` int NOT NULL,
  `act_input` int NOT NULL,
  `act_edit` int NOT NULL,
  `act_delete` int NOT NULL,
  `act_additional` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table aptavis.tbl_user_akses: 40 rows
DELETE FROM `tbl_user_akses`;
/*!40000 ALTER TABLE `tbl_user_akses` DISABLE KEYS */;
INSERT INTO `tbl_user_akses` (`id`, `id_group`, `id_menu`, `act_view`, `act_input`, `act_edit`, `act_delete`, `act_additional`) VALUES
	(1, 1, 1, 1, 0, 0, 0, 0),
	(2, 1, 2, 1, 0, 0, 0, 0),
	(3, 1, 3, 1, 1, 1, 1, 0),
	(4, 1, 4, 1, 0, 0, 0, 0),
	(5, 1, 5, 1, 0, 0, 0, 0),
	(6, 1, 6, 1, 1, 1, 1, 0),
	(7, 1, 7, 1, 1, 1, 1, 0),
	(8, 1, 8, 1, 1, 1, 1, 0),
	(9, 1, 9, 1, 1, 1, 1, 0),
	(10, 1, 10, 1, 1, 1, 1, 0),
	(11, 1, 11, 1, 0, 0, 0, 0),
	(12, 1, 12, 1, 1, 0, 1, 1),
	(13, 1, 13, 1, 0, 0, 0, 1),
	(14, 1, 14, 1, 0, 0, 0, 0),
	(15, 2, 17, 1, 0, 0, 0, 0),
	(16, 2, 18, 1, 1, 1, 1, 0),
	(17, 2, 19, 1, 1, 1, 1, 0),
	(18, 2, 15, 0, 0, 0, 0, 0),
	(19, 2, 16, 1, 0, 0, 0, 0),
	(21, 2, 21, 0, 0, 0, 0, 0),
	(22, 2, 5, 1, 0, 0, 0, 0),
	(23, 2, 6, 1, 1, 1, 1, 0),
	(24, 2, 7, 0, 0, 0, 0, 0),
	(25, 2, 1, 0, 0, 0, 0, 0),
	(26, 2, 2, 0, 0, 0, 0, 0),
	(27, 2, 3, 0, 0, 0, 0, 0),
	(28, 2, 4, 0, 0, 0, 0, 0),
	(29, 2, 8, 0, 0, 0, 0, 0),
	(30, 2, 10, 0, 0, 0, 0, 0),
	(31, 2, 9, 0, 0, 0, 0, 0),
	(32, 2, 11, 0, 0, 0, 0, 0),
	(33, 2, 12, 0, 0, 0, 0, 0),
	(34, 2, 13, 0, 0, 0, 0, 0),
	(35, 2, 14, 0, 0, 0, 0, 0),
	(36, 1, 17, 1, 0, 0, 0, 0),
	(37, 1, 18, 1, 1, 1, 1, 0),
	(38, 1, 19, 1, 1, 1, 1, 0),
	(39, 1, 15, 0, 0, 0, 0, 0),
	(40, 1, 16, 1, 0, 0, 0, 0),
	(42, 1, 21, 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `tbl_user_akses` ENABLE KEYS */;

-- Dumping structure for table aptavis.tbl_user_group
CREATE TABLE IF NOT EXISTS `tbl_user_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `keterangan` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `create_by` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL,
  `update_by` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Dumping data for table aptavis.tbl_user_group: 2 rows
DELETE FROM `tbl_user_group`;
/*!40000 ALTER TABLE `tbl_user_group` DISABLE KEYS */;
INSERT INTO `tbl_user_group` (`id`, `nama`, `keterangan`, `is_active`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
	(1, 'Dev', 'Khusus Developer', 1, 'Ibeffaro', '2023-06-05 21:54:10', 'Ibeffaro', '2023-10-25 15:59:05'),
	(2, 'Superadmin', 'Role khusus untuk pengguna Superadmin', 1, 'Ibeffaro', '2023-06-05 21:54:10', 'Ibeffaro', '2023-10-25 15:59:11');
/*!40000 ALTER TABLE `tbl_user_group` ENABLE KEYS */;

-- Dumping structure for table aptavis.tbl_user_log
CREATE TABLE IF NOT EXISTS `tbl_user_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(30) NOT NULL,
  `id_user` int NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL,
  `keterangan` text NOT NULL,
  `metode` varchar(5) NOT NULL,
  `data` text NOT NULL,
  `respon` int NOT NULL COMMENT '200:Sukses|400:Gagal Login|403:Forbidden|404:Page Not Found',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table aptavis.tbl_user_log: 0 rows
DELETE FROM `tbl_user_log`;
/*!40000 ALTER TABLE `tbl_user_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_user_log` ENABLE KEYS */;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
