<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends BE_Controller {

    private $bg_app = [
        'x_red.css'     => '#E06666',
        'x_blue.css'    => '#284D70'
    ];

    public function __construct() {
        parent:: __construct();
    }

    public function index() {
        $data['title']      = lang('dashboard');
        $data['pengumuman'] = get_data('tbl_pengumuman a',array(
            'select'    => 'a.pengumuman,b.nama',
            'join'      => 'tbl_user b on a.id_user = b.id type left',
            'where'     => array(
                'a.tanggal_publish <='  => date('Y-m-d H:i:s'),
                'a.tanggal_selesai >='  => date('Y-m-d H:i:s'),
                'a.is_active'           => 1
            ),
            'sort_by'   => 'a.update_at',
            'sort'      => 'desc'
        ))->result_array();
        $bgapp                  = setting('warna_primary');
        if(setting('theme_alt') && isset($this->bg_app[setting('theme_alt')])) {
            $bgapp              = $this->bg_app[setting('theme_alt')];
        }
        list($r, $g, $b)        = sscanf($bgapp, "#%02x%02x%02x");
        $data['bgapp']          = "rgba($r, $g, $b, .8)";

        render($data);
    }

    function info() {
        $data['pengumuman'] = get_data('tbl_pengumuman a',array(
            'select'    => 'a.pengumuman,b.nama,b.foto,a.create_at',
            'join'      => array(
                'tbl_user b'    => array(
                    'on'    => 'a.id_user = b.id',
                    'type'  => 'left'
                )
            ),
            'where_array'   => array(
                'a.tanggal_publish <='  => date('Y-m-d H:i:s'),
                'a.tanggal_selesai >='  => date('Y-m-d H:i:s'),
                'a.is_active'           => 1
            ),
            'sort_by'   => 'a.update_at',
            'sort'      => 'desc'
        ))->result();
        render($data,'layout:false');        
    }

}