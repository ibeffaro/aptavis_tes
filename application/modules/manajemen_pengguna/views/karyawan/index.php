<div class="content-header">
	<div class="main-container position-relative">
		<div class="header-info">
			<div class="content-title"><?php echo $title; ?></div>
			<?php echo breadcrumb(); ?>
		</div>
		<div class="float-right">
			<?php echo access_button('delete'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="content-body">
	<?php
	table_open('', true, base_url('manajemen_pengguna/karyawan/data'), 'tbl_karyawan');
	thead();
	tr();
	th('checkbox', 'text-center', 'width="30" data-content="id"');
	th('Foto', '', 'data-content="url_foto" data-type="image-url" data-filter="false" width="100" data-sort="false"');
	th('Nama karyawan', '', 'data-content="nama_karyawan"');
	th('Tanggal Lahir', '', 'data-content="tanggal_lahir" data-type="date"');
	th('Jabatan', '', 'data-content="jabatan"');
	th(lang('aktif') . '?', 'text-center', 'width="120px" data-content="is_active" data-type="boolean" data-filter="false"');
	th('&nbsp;', '', 'width="30" data-content="action_button"');
	table_close();
	table_close();
	?>
</div>
<?php
modal_open('modal-form', '', '', 'data-openCallback="formOpen"');
modal_body();
form_open(base_url('manajemen_pengguna/karyawan/save'), 'post', 'form');
col_init(3, 9);
input('hidden', 'id', 'id');
input('text', 'Nama Karyawan', 'nama_karyawan', 'required');
input('date', 'Tanggal Lahir', 'tanggal_lahir', 'required');
select2('Jabatan', 'id_jabatan', 'required', $jabatan, 'id', 'nama');
input('text', 'Url Foto', 'url_foto', '');
toggle('Have User Login?', 'have_user_login', false);
sub_open(1);
input('text', 'Username', 'username', 'min-length:4|unique|alphanumeric');
input('password', 'Password', 'password');
sub_close();
toggle(lang('aktif') . '?', 'is_active');
form_button(lang('simpan'), lang('batal'));
form_close();
modal_footer();
modal_close();
?>
<script>
	function formOpen() {
		$('#username,#password').removeAttr('data-validation', 'required').closest('.form-group').addClass('d-none');
		if (typeof response_edit.id != "undefined") {
			console.log(response_edit);
			if (response_edit.have_user_login == '1') {
				$('#username').attr('data-validation', 'required').closest('.form-group').removeClass('d-none');
				$('#password').closest('.form-group').removeClass('d-none');
				$('#username').val(response_edit.username);
			}
		}
	}

	$('#have_user_login').click(function() {
		if ($(this).is(':checked')) {
			$('#username').attr('data-validation', 'required').closest('.form-group').removeClass('d-none');
			$('#password').attr('data-validation', 'required').closest('.form-group').removeClass('d-none');
		} else {
			$('#username').val('').removeAttr('data-validation', 'required').closest('.form-group').addClass('d-none');
			$('#password').val('').removeAttr('data-validation', 'required').closest('.form-group').addClass('d-none');
		}
	});
</script>