<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Karyawan extends BE_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$data['jabatan']	= get_data('tbl_master', ['where' => ['id_master' => 24, 'is_active' => 1]])->result_array();
		render($data);
	}

	function data()
	{
		$config = [
			'access_view' => false
		];
		$data	= data_serverside($config);
		render($data, 'json');
	}

	function get_data()
	{
		$data 	= get_data('tbl_karyawan', 'id', post('id'))->row_array();
		$user 	= get_data('tbl_user', [
			'select' 	=> 'id, id_karyawan, username',
			'where' 	=> ['id_karyawan' => post('id')]
		])->row_array();
		$data['username'] = isset($user['username']) ? $user['username'] : '';
		render($data, 'json');
	}

	function save()
	{
		$data 		= post();
		$mJabatan	= get_data('tbl_master', 'id', $data['id_jabatan'])->row();
		$username  	= $data['username'];
		$password 	= $data['password'];
		unset($data['username']);
		unset($data['password']);

		$data['create_by_id'] 	= user('id');
		$data['jabatan']		= isset($mJabatan->nama) ? $mJabatan->nama : '';
		if ($data['id']) $data['update_by_id'] = user('id');

		$response  	= save_data('tbl_karyawan', $data, post(':validation'));
		if ($response['status'] == 'success') {
			$cekUser = get_data('tbl_user', [
				'select' 	=> 'id, id_karyawan, id_group, nama',
				'where' 	=> ['id_karyawan' => $response['id']]
			])->row_array();
			$idKaryawan = isset($cekUser->id_karyawan) && $cekUser->id_karyawan ? $cekUser->id_karyawan : 0;
			if ($data['have_user_login'] == 1) {
				if (!$idKaryawan) {
					$dataUser = [
						'id_karyawan' 	=> $response['id'],
						'id_group' 		=> 3, // Karyawan
						'nama' 			=> $data['nama_karyawan'],
						'kategori_user' => 'U',
						'username'		=> $username,
						'password'		=> $password,
						'is_active' 	=> 1
					];
					insert_data('tbl_user', $dataUser);
				} else {
					$dataUser = [
						'id_karyawan' 	=> $cekUser['id_karyawan'],
						'id_group' 		=> $cekUser['id_group'],
						'nama' 			=> $data['nama'],
						'kategori_user' => $cekUser['kategori_user'],
						'username'		=> $username,
						'is_active' 	=> $data['is_active']
					];
					if ($data['password']) $dataUser['password'] = $password;
					update_data('tbl_user', $dataUser, 'id_karyawan', $response['id']);
				}
			} else {
				if ($idKaryawan) {
					delete_data('tbl_user', 'id_karyawan', $cekUser['id_karyawan']);
				}
			}
		}
		render($response, 'json');
	}

	function delete()
	{
		$response = destroy_data('tbl_karyawan', 'id', post('id'));
		render($response, 'json');
	}
}
