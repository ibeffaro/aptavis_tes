<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_lists extends BE_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$m 						= json_decode(setting('mandatory_role'), true);
		$mandatory_role 		= [0];
		if ($m != null && is_array($m) && count($m) > 0) {
			$mandatory_role 	= $m;
		}
		if (user('id_group') != 1) {
			$mandatory_role[] 	= 1;
		}
		$data['group']			= get_data('tbl_user_group', [
			'where'				=> [
				'is_active'		=> 1,
				'id !='			=> $mandatory_role
			]
		])->result_array();
		render($data);
	}

	function data()
	{
		$m 						= json_decode(setting('mandatory_role'), true);
		$mandatory_role 		= [0];
		if ($m != null && is_array($m) && count($m) > 0) {
			$mandatory_role 	= $m;
		}
		if (user('id_group') != 1) {
			$mandatory_role[] 	= 1;
		}
		$config['access_view'] 	= false;
		$config['where']['id_group !=']	= $mandatory_role;
		if (setting('jumlah_salah_password')) {
			$config['button']	= button_serverside('btn-success', 'btn-unlock', ['fa-unlock', lang('buka_kunci'), true], 'act-unlock', ['invalid_password >=' => setting('jumlah_salah_password')]);
		}
		$data 					= data_serverside($config);
		render($data, 'json');
	}

	function get_data()
	{
		$data 					= get_data('tbl_user', 'id', post('id'))->row_array();
		render($data, 'json');
	}

	function save()
	{
		$data 					= post();
		$data['kategori_user']	= 'U';

		if (isset($data['telepon']) && $data['telepon']) {
			$data['telepon']	= str_replace(['(', ')', ' ', '-'], '', $data['telepon']);
			if (substr($data['telepon'], 0, 3) == '+62') $data['telepon'] = substr($data['telepon'], 1);
			else if (substr($data['telepon'], 0, 1) == '0') $data['telepon'] = '62' . substr($data['telepon'], 1);
			else if (substr($data['telepon'], 0, 2) != '62') $data['telepon'] = '62' . $data['telepon'];
		}
		$response 				= save_data('tbl_user', $data, post(':validation'));
		if ($response['status'] == 'success' && post('password')) {
			update_data('tbl_user', [
				'change_password_by'    => user('nama'),
				'change_password_at'    => date('Y-m-d H:i:s')
			], 'id', $response['id']);
			$check  			= get_data('tbl_history_password', [
				'where' 		=> [
					'id_user'   => $response['id'],
					'password'  => md5(post('password'))
				]
			])->row();
			if (isset($check->id)) {
				update_data('tbl_history_password', ['tanggal' => date('Y-m-d H:i:s')], 'id', $check->id);
			} else {
				insert_data('tbl_history_password', [
					'id_user'   => $response['id'],
					'password'  => md5(post('password')),
					'tanggal'   => date('Y-m-d H:i:s')
				]);
			}
		}
		render($response, 'json');
	}

	function delete()
	{
		$response = destroy_data('tbl_user', 'id', post('id'), '', 'foto');
		render($response, 'json');
	}

	function template()
	{
		$arr            = [
			'kode'		=> 'ID Pengguna (Jika tidak diisi tidak akan disave)',
			'nama'		=> 'Nama',
			'email'		=> 'Email',
			'telepon'	=> 'Telepon',
			'username'	=> 'Username (Jika tidak diisi akan otomatis ID Pengguna menjadi username)',
			'password'	=> 'Password',
			'role'		=> 'Role'
		];
		$config[]		= [
			'title'		=> 'template_import_user',
			'header'	=> $arr
		];
		$this->load->library('simpleexcel', $config);
		$this->simpleexcel->export();
	}

	function import()
	{
		ini_set('memory_limit', '-1');

		$m 						= json_decode(setting('mandatory_role'), true);
		$mandatory_role 		= [0];
		if ($m != null && is_array($m) && count($m) > 0) {
			$mandatory_role 	= $m;
		}
		if (user('id_group') != 1) {
			$mandatory_role[] 	= 1;
		}

		$this->load->library('PHPExcel');
		$file   = post('fileimport');
		try {
			$objPHPExcel = PHPExcel_IOFactory::load($file);
		} catch (Exception $e) {
			die('Error loading file :' . $e->getMessage());
		}
		$worksheet  = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
		$numRows    = count($worksheet);
		$tambah     = 0;
		$edit       = 0;
		for ($i = 1; $i < ($numRows + 1); $i++) {
			if (strtoupper($worksheet[$i]["A"]) !== "KODE" && strtoupper($worksheet[$i]["B"]) !== "NAMA") {
				$data = [
					"kode"          => strtoupper(trim($worksheet[$i]["A"], " ")),
					"nama"          => trim($worksheet[$i]["B"], " "),
					"email"         => trim($worksheet[$i]["C"], " "),
					"telepon"     	=> trim($worksheet[$i]["D"], " "),
					"username"      => trim($worksheet[$i]["E"], " "),
					"password"      => trim($worksheet[$i]["F"], " "),
					"id_group"      => trim($worksheet[$i]["G"], " "),
					"is_active"  	=> 1
				];
				if ($data['kode']) {
					$data['username']   = $data['username'] ? $data['username'] : $data['kode'];
					$group              = get_data('tbl_user_group', [
						'where'			=> [
							'nama'		=> $data['id_group'],
							'id !='		=> $mandatory_role
						]
					])->row();
					$data['id_group']   = isset($group->id) ? $group->id : 0;
					$data['password']   = $data['password'] ?
						password_hash(md5($data['password']), PASSWORD_DEFAULT, ['cost' => COST]) :
						password_hash(md5($data['kode']), PASSWORD_DEFAULT, ['cost' => COST]);
					$check  			= get_data('tbl_user', [
						'where'			=> [
							'kode'		=> $data['kode']
						]
					])->row();
					if (isset($check->id)) {
						unset($data['username']);
						$data['update_by']	= user('nama');
						$data['update_at']	= date('Y-m-d H:i:s');
						$save   = update_data('tbl_user', $data, 'id', $check->id);
						if ($save) {
							$edit++;
							$save = $check->id;
						}
					} else {
						$username   		= get_data('tbl_user', [
							'where'			=> [
								'username'	=> $data['username']
							]
						])->row();
						if (isset($username->id)) $data['username'] = $data['kode'];
						$data['create_by'] 	= user('nama');
						$data['create_at']	= date('Y-m-d H:i:s');
						$save   = insert_data('tbl_user', $data);
						if ($save) {
							$tambah++;
						}
					}
				}
			}
		}
		delete_dir(FCPATH . "assets/uploads/temp/" . md5(user('id') . 'fileimport'));
		$response = [
			'status' 	=> 'success',
			'message' 	=> $tambah . ' ' . lang('data_berhasil_disimpan') . ', ' . $edit . ' ' . lang('data_berhasil_diperbaharui')
		];
		render($response, 'json');
	}

	function export()
	{
		$header 		= [
			'kode'		=> 'ID Pengguna',
			'nama'		=> 'Nama',
			'email'		=> 'Email',
			'telepon'	=> 'Telepon',
			'username'	=> 'Username',
			'grp'		=> 'Role'
		];
		$user   		= get_data('tbl_user a', [
			'select'    => 'a.*,b.nama AS grp',
			'join'      => [
				'tbl_user_group b on a.id_group = b.id type left',
			]
		])->result_array();
		$config			= [
			'title'		=> 'data_user',
			'header'	=> $header,
			'data'		=> $user
		];
		$this->load->library('simpleexcel', $config);
		$this->simpleexcel->export();
	}

	function unlock()
	{
		$data = [
			'id'				=> post('id'),
			'invalid_password'	=> 0
		];
		$res = save_data('tbl_user', $data, [], true);
		render($res, 'json');
	}
}
