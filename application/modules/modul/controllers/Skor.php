<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Skor extends BE_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$data['klub'] = get_data('tbl_data_klub','is_active',1)->result_array();
		render($data);
	}

	function data()
	{
		$config = [
			'access_view' => false
		];
		$data	= data_serverside($config);
		render($data, 'json');
	}

	function get_data()
	{
		$data = get_data('tbl_data_skor', 'id', post('id'))->row_array();
		render($data, 'json');
	}

	function save()
	{
		$data 		= post();
		if($data['id_klub1'] == $data['id_klub2']){
			render([
				'status'	=> 'failed',
				'message'	=> 'Klub I tidak boleh sama dengan Klub II ataupun sebaliknya'
			],'json');
			die;
		}
		$cekTanding 	= get_data('tbl_data_skor',[
			'where' 	=> [
				'id_klub1' => $data['id_klub1'],
				'id_klub2' => $data['id_klub2']
			]
		])->row();
		if(isset($cekTanding->id) && $cekTanding->id){
			render([
				'status'	=> 'failed',
				'message'	=> 'Data pertandingan tidak boleh sama'
			],'json');
			die;
		}
		$getKLub 		= get_data('tbl_data_klub','id',$data['id_klub1'])->row();
		$data['klub1'] 	= isset($getKLub->nama_klub) ? $getKLub->nama_klub : ''; 
		$getKLub2 		= get_data('tbl_data_klub','id',$data['id_klub2'])->row();
		$data['klub2'] 	= isset($getKLub2->nama_klub) ? $getKLub2->nama_klub : ''; 
		$response 		= save_data('tbl_data_skor', $data, post(':validation'));
		render($response, 'json');
	}

	function delete()
	{
		$response = destroy_data('tbl_data_skor', 'id', post('id'));
		render($response, 'json');
	}
}
