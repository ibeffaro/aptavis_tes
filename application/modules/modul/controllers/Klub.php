<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Klub extends BE_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		render();
	}

	function data()
	{
		$config = [
			'access_view' => false
		];
		$data	= data_serverside($config);
		render($data, 'json');
	}

	function get_data()
	{
		$data = get_data('tbl_data_klub', 'id', post('id'))->row_array();
		render($data, 'json');
	}

	function save()
	{
		$data 		= post();
		$response 	= save_data('tbl_data_klub', $data, post(':validation'));
		render($response, 'json');
	}

	function delete()
	{
		$response = destroy_data('tbl_data_klub', 'id', post('id'));
		render($response, 'json');
	}
}
