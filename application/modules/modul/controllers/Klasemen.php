<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Klasemen extends BE_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$getKlub = get_data('tbl_data_skor')->result_array();
		foreach ($getKlub as $k => $v) {
			$klub1[$k]	= $v['klub1'];
			$klub2[$k] 	= $v['klub2'];

			$hasilPertandingan[$k] = [
				'home' 		=> strtolower($v['klub1']),
				'away' 		=> strtolower($v['klub2']),
				'skor_home' => $v['skor1'],
				'skor_away' => $v['skor2']
			];
		}

		$mergeKlub 	= array_merge($klub1,$klub2);
		$klub 		= [];
		foreach ($mergeKlub as $mk) {
			$klub[strtolower($mk)] = $mk;
		}

		$record = [];
		foreach ($klub as $k => $nama) {
			$main 		= 0;
			$menang 	= 0;
			$seri 		= 0;
			$kalah 		= 0;
			$goalMenang = 0;
			$goalKalah 	= 0;

			foreach ($hasilPertandingan as $hp) {
				if($hp['home'] == $k) {
					$main++;
					$goalMenang += $hp['skor_home'];
					$goalKalah	+= $hp['skor_away'];

					if ($hp['skor_home'] > $hp['skor_away']) {
                        $menang++;
                    } elseif ($hp['skor_home'] == $hp['skor_away']) {
                        $seri++;
                    } else {
                        $kalah++;
                    }
				} elseif ($hp['away'] == $k) {
                    $main++;
                    $goalMenang += $hp['skor_away'];
                    $goalKalah 	+= $hp['skor_home'];

                    if ($hp['skor_away'] > $hp['skor_home']) {
                        $menang++;
                    } elseif ($hp['skor_away'] == $hp['skor_home']) {
                        $seri++;
                    } else {
                        $kalah++;
                    }
                }
			}
			$point = $menang * 3 + $seri;

            $record[] = [
                'nama' 			=> $nama,
                'main' 			=> $main,
                'menang' 		=> $menang,
                'seri' 			=> $seri,
                'kalah' 		=> $kalah,
                'goal_menang' 	=> $goalMenang,
                'goal_kalah' 	=> $goalKalah,
                'point' 		=> $point
            ];
		}
		$data['record'] = $record;
		render($data);
	}

	
}
