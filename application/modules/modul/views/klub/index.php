<div class="content-header">
	<div class="main-container position-relative">
		<div class="header-info">
			<div class="content-title"><?php echo $title; ?></div>
			<?php echo breadcrumb(); ?>
		</div>
		<div class="float-right">
			<?php echo access_button('delete'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="content-body">
	<?php
	table_open('', true, base_url('modul/klub/data'), 'tbl_data_klub');
	thead();
	tr();
	th('checkbox', 'text-center', 'width="30" data-content="id"');
	th('Nama Klub', '', 'data-content="nama_klub"');
	th('Kota', '', 'data-content="kota"');
	th(lang('aktif') . '?', 'text-center', 'width="120px" data-content="is_active" data-type="boolean"');
	th('&nbsp;', '', 'width="30" data-content="action_button"');
	table_close();
	table_close();
	?>
</div>
<?php
modal_open('modal-form');
modal_body();
form_open(base_url('modul/klub/save'), 'post', 'form');
col_init(3, 9);
input('hidden', 'id', 'id');
input('text', 'Nama Klub', 'nama_klub', 'required|unique');
input('text', 'Kota', 'kota', 'required|unique');
toggle(lang('aktif') . '?', 'is_active');
form_button(lang('simpan'), lang('batal'));
form_close();
modal_footer();
modal_close();
?>