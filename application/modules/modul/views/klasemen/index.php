<div class="content-header">
	<div class="main-container position-relative">
		<div class="header-info">
			<div class="content-title"><?php echo $title; ?></div>
			<?php echo breadcrumb(); ?>
		</div>
		<div class="float-right">
			<?php echo access_button('delete'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="content-body">
	<div class="main-container">
		<div class="row pt-2">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header pt-2 pb-2 pr-3 pl-3">
						<div class="float-left pt-2 mb-2 mb-sm-0">Data Klasemen</div>
						<div class="clearfix"></div>
					</div>
					<div class="card-body p-3">
						<div class="table-responsive">
							<table class="table table-bordered table-app table-hover">
								<thead>
									<tr class="text-center">
										<th width="10">No</th>
										<th>Klub</th>
										<th>Ma</th>
										<th>Me</th>
										<th>S</th>
										<th>K</th>
										<th>GM</th>
										<th>GK</th>
										<th>Point</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($record as $k => $v) { ?>
										<tr class="text-center">
											<td><?php echo $k + 1; ?></td>
											<td><?php echo $v['nama']; ?></td>
											<td><?php echo $v['main']; ?></td>
											<td><?php echo $v['menang']; ?></td>
											<td><?php echo $v['seri']; ?></td>
											<td><?php echo $v['kalah']; ?></td>
											<td><?php echo $v['goal_menang']; ?></td>
											<td><?php echo $v['goal_kalah']; ?></td>
											<td><?php echo $v['point']; ?></td>
										</tr>
									<?php }  ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>