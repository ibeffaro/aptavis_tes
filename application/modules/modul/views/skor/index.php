<div class="content-header">
	<div class="main-container position-relative">
		<div class="header-info">
			<div class="content-title"><?php echo $title; ?></div>
			<?php echo breadcrumb(); ?>
		</div>
		<div class="float-right">
			<?php echo access_button('delete'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="content-body">
	<?php
	table_open('', true, base_url('modul/skor/data'), 'tbl_data_skor');
	thead();
	tr();
	th('checkbox', 'text-center', 'width="30" data-content="id"');
	th('Klub I', '', 'data-content="klub1"');
	th('Skor', 'text-center', 'data-content="skor1"');
	th('Klub II', '', 'data-content="klub2"');
	th('Skor', 'text-center', 'data-content="skor2"');
	th('&nbsp;', '', 'width="30" data-content="action_button"');
	table_close();
	table_close();
	?>
</div>
<?php
modal_open('modal-form');
modal_body();
form_open(base_url('modul/skor/save'), 'post', 'form');
col_init(3, 9);
input('hidden', 'id', 'id'); ?>
<div class="form-group row">
	<label class="col-lg-3 col-form-label required">Klub I</label>
	<div class="col-lg-6">
		<select name="id_klub1" id="id_klub1" class="form-control select2" data-validation="required">
			<option value=""></option>
			<?php foreach ($klub as $k) { ?>
				<option value="<?php echo $k['id']; ?>"><?php echo $k['nama_klub']; ?></option>
			<?php } ?>
		</select>
	</div>
	<div class="col-lg-3">
		<input type="text" name="skor1" id="skor1" class="form-control" data-validation="number" placeholder="Skor" autocomplete="off" />
	</div>
</div>
<div class="form-group row">
	<label class="col-lg-3 col-form-label required">Klub II</label>
	<div class="col-lg-6">
		<select name="id_klub2" id="id_klub2" class="form-control select2" data-validation="required">
			<option value=""></option>
			<?php foreach ($klub as $k) { ?>
				<option value="<?php echo $k['id']; ?>"><?php echo $k['nama_klub']; ?></option>
			<?php } ?>
		</select>
	</div>
	<div class="col-lg-3">
		<input type="text" name="skor2" id="skor2" class="form-control" data-validation="number" placeholder="Skor" autocomplete="off" />
	</div>
</div>
<?php 
form_button(lang('simpan'), lang('batal'));
form_close();
modal_footer();
modal_close();
?>
