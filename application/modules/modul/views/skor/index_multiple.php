<div class="content-header">
	<div class="main-container position-relative">
		<div class="header-info">
			<div class="content-title"><?php echo $title; ?></div>
			<?php echo breadcrumb(); ?>
		</div>
		<div class="float-right">
			<?php echo access_button('delete'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="content-body">
	<?php
	table_open('', true, base_url('modul/skor/data'), 'tbl_data_skor');
	thead();
	tr();
	th('checkbox', 'text-center', 'width="30" data-content="id"');
	th('Klub', '', 'data-content="id_klub"');
	th('Skor', '', 'data-content="skor"');
	th('&nbsp;', '', 'width="30" data-content="action_button"');
	table_close();
	table_close();
	?>
</div>
<div class="d-none" id="data-master">
	<select id="data-klub">
		<option value=""></option>
		<?php foreach($klub as $c) { ?>
		<option value="<?php echo $c['id']; ?>"><?php echo $c['nama_klub']; ?></option>
		<?php } ?>
	</select>
</div>
<?php
modal_open('modal-form','','modal-lg','data-openCallback="formOpen"');
modal_body();
form_open(base_url('modul/skor/save'), 'post', 'form');
col_init(3, 9);
input('hidden', 'id', 'id'); 
echo '<div class="col-md-12 mb-3">';
label('Klub dan Skor');
?>
<div id="data-skor"></div>
<div class="row mt-2">
	<div class="col-12 col-xl-2 offset-xl-10">
		<button type="button" class="btn btn-block btn-success btn-icon-only" id="btn-add"><i class="fa-plus"></i></button>							
	</div>
</div>
<?php
echo '</div>';
form_button(lang('simpan'), lang('batal'));
form_close();
modal_footer();
modal_close();
?>

<script>
$('#btn-add').click(function(){
	var opt = '';
		opt = '<option value=""></option>';
		opt = $('#data-klub').html();
	var konten = 	'<div class="row form-group list-klub">' +
						'<div class="col-12 col-xl-5">' +
							'<div class="row">' +
								'<div class="col-xl-6 mb-2 mb-xl-5">' +
									'<select name="id_klub[]" class="form-control select2 infinity" data-validation="required|unique" data-placeholder="Pilih Klub 1">' +
										opt +
									'</select>' +
								'</div>' +
								'<div class="col-12 col-xl-3 mb-2">' +
									'<input type="text" name="skor[]" class="form-control money" autocomplete="off" data-validation="required|number|unique" placeholder="Skor 1" />' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<div class="col-12 col-xl-5">' +
							'<div class="row">' +
								'<div class="col-xl-6 mb-2 mb-xl-5">' +
									'<select name="id_klub2[]" class="form-control select2 infinity" data-validation="required|unique" data-placeholder="Pilih Klub 2">' +
										opt +
									'</select>' +
								'</div>' +
								'<div class="col-12 col-xl-3">' +
									'<input type="text" name="skor2[]" class="form-control money" autocomplete="off" data-validation="required|number|unique" placeholder="Skor 2" />' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<div class="col-2 col-xl-2">' +
							'<button type="button" class="btn btn-block btn-danger btn-icon-only btn-remove"><i class="fa-times"></i></button>' +
						'</div>' +
					'</div>';
	$('#data-skor').append(konten);
	money_init();
	$('#data-skor').find('.select2').last().select2({
		templateResult: formatSelect2,
		templateSelection: formatSelect2,
		// placeholder: 'Pilih Nama Klub',
		dropdownParent : $(this).parent()
	});
	select2Trigger();
});
$(document).on('click','.btn-remove',function(){
	if($('#data-skor .form-group').length > 1) {
		$(this).closest('.form-group').remove();
	}
});
function formOpen() {
	$('.list-klub').remove();
	$('#btn-add').trigger('click');
	if(typeof response_edit.id != 'undefined') {
		

		response_edit = {};
	}
}
</script>