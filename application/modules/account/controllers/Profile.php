<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends BE_Controller {

	function __construct() {
		parent::__construct();
	}

	function index() {
		$data['title'] = lang('profil');
		render($data);
	}

	function save() {
		$data = post();
		if(isset($data['telepon']) && $data['telepon']) {
			$data['telepon']	= str_replace(['(',')',' ','-'],'',$data['telepon']);
			if(substr($data['telepon'],0,3) == '+62') $data['telepon'] = substr($data['telepon'],1);
			else if(substr($data['telepon'],0,1) == '0') $data['telepon'] = '62' . substr($data['telepon'],1);
			else if(substr($data['telepon'],0,2) != '62') $data['telepon'] = '62' . $data['telepon'];
		}
		$response = save_data('tbl_user', $data, post(':validation'));
		render($response,'json');
	}
}