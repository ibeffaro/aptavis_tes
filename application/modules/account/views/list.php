<div class="show-panel sticky-top">
	<div class="card">
		<div class="card-header">
			<?php echo lang('pengaturan_akun'); ?>
		</div>
		<div class="card-body dropdown-menu">
			<a class="dropdown-item<?php if($uri_string == 'account/profile') echo ' active'; ?>" href="<?php echo base_url('account/profile'); ?>"><i class="fa-user-edit"></i><?php echo lang('profil');?></a>
			<a class="dropdown-item<?php if($uri_string == 'account/changepwd') echo ' active'; ?>" href="<?php echo base_url('account/changepwd'); ?>"><i class="fa-key"></i><?php echo lang('ubah_kata_sandi'); ?></a>
			<?php if(user('kode') == user('_key')) { ?>
			<a class="dropdown-item<?php if($uri_string == 'account/acc_settings') echo ' active'; ?>" href="<?php echo base_url('account/acc_settings'); ?>"><i class="fa-cog"></i><?php echo lang('pengaturan');?></a>
			<?php } ?>
		</div>
	</div>
</div>