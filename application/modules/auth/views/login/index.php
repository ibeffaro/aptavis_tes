<div class="text-center pt-2 pb-2">
	<a href="<?php echo base_url(); ?>">
		<img src="<?php echo setting('logo_alt') ?: base_url(dir_upload('setting') . setting('logo')); ?>" alt="<?php setting('title'); ?>" class="logo-auth">
	</a>
</div>
<form method="post" class="login-wrapper" action="<?php echo base_url('auth/login/do_login'); ?>" id="form">
	<div class="fieldset">
		<label class="inline-icon fa-user" for="username"><?php echo lang('nama_pengguna'); ?></label>
		<input type="text" class="form-control form-control-lg" id="username" name="username" placeholder="<?php echo lang('nama_pengguna'); ?>" autocomplete="off" data-validation="required">
	</div>
	<div class="fieldset">
		<label class="inline-icon fa-lock" for="password"><?php echo lang('kata_sandi'); ?></label>
		<input type="password" class="form-control form-control-lg password" id="password" name="password" placeholder="<?php echo lang('kata_sandi'); ?>" autocomplete="off" data-validation="required">
		<button type="button" class="hide-password"><i class="fa-eye"></i></button>
	</div>
	<button type="submit" class="btn btn-primary btn-block"><?php echo lang('masuk'); ?></button>
</form>
<script type="text/javascript">
	$(document).ready(function() {
		localStorage.clear();
	});
	$('.hide-password').click(function() {
		if ($('#password').attr('type') == 'text') {
			$('#password').attr('type', 'password').focus();
			$('.hide-password i').removeClass('fa-eye-slash').addClass('fa-eye');
		} else {
			$('#password').attr('type', 'text').focus();
			$('.hide-password i').removeClass('fa-eye').addClass('fa-eye-slash');
		}
	});
	$('#form').submit(function(e) {
		e.preventDefault();
		if (validation()) {
			$.ajax({
				url: $(this).attr('action'),
				data: $(this).serialize(),
				type: 'POST',
				dataType: 'json',
				success: function(response) {
					if (response.status == 'success') {
						window.location = response.redirect;
					} else {
						cAlert.open(response.message, response.status);
					}
				}
			});
		}
	});
</script>