<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo isset($id) ? $id_pasien : lang('halaman_tidak_ditemukan'); ?></title>
    <link href="<?php echo base_url(dir_upload('setting') . setting('favicon')); ?>" rel="shortcut icon" />
    <?php
    Asset::css('bootstrap.min.css', true);
    Asset::css('bootstrap.color.min.css', true);
    Asset::css('roboto.css', true);
    Asset::css('fontawesome.css', true);
    Asset::css('style.css', true);
    Asset::css('small-style.css', true);
    echo Asset::render();
    ?>
</head>

<body class="bg-grey p-0">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col col-md-10 col-lg-8 mt-4 mb-4">
                <div class="card mb-4">
                    <div class="card-header p-4 bg-light-grey">
                        <table width="100%">
                            <tr>
                                <td width="70"><img src="<?php echo $logo; ?>" width="100%" /></td>
                                <td style="padding-left: 10px; vertical-align: middle;">
                                    <div style="font-size: 14px; font-weight: bold;"><?php echo $nama_perusahaan; ?></div>
                                    <div><?php echo str_replace("\n", '<br />', $alamat_perusahaan); ?></div>
                                    <div>
                                        <?php
                                        if ($telp_perusahaan) echo 'Telp: ' . $telp_perusahaan . ' &nbsp; ';
                                        if ($email_perusahaan) echo 'Email: ' . $email_perusahaan;
                                        ?>
                                    </div>
                                </td>
                                <td>
                                    <div style="font-size: 24px; font-weight: bold; text-align: right; color: #bbb;"><?php echo $id_pasien; ?></div>
                                    <div style="text-align: right;">KARTU PASIEN</div>
                                    <div style="text-align: right;">Tanggal : <?php echo date('d/m/Y', strtotime($create_at)); ?></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="card-body p-4">
                        <div class="mb-3">
                            <table width="75%">
                                <tr>
                                    <th>Nama</th>
                                    <th>:</th>
                                    <td><?php echo $nama; ?></td>
                                </tr>
                                <tr>
                                    <th>Alamat</th>
                                    <th>:</th>
                                    <td><?php echo $alamat; ?></td>
                                </tr>
                                <tr>
                                    <th>Telepon</th>
                                    <th>:</th>
                                    <td><?php echo $telepon; ?></td>
                                </tr>
                                <tr>
                                    <th>RT/RW</th>
                                    <th>:</th>
                                    <td><?php echo $rt_rw; ?></td>
                                </tr>
                                <tr>
                                    <th>Kelurahan</th>
                                    <th>:</th>
                                    <td><?php echo $kelurahan; ?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Lahir</th>
                                    <th>:</th>
                                    <td><?php echo date('d/m/Y', strtotime($tgl_lahir)); ?></td>
                                </tr>
                                <tr>
                                    <th>Jenis Kelamin</th>
                                    <th>:</th>
                                    <td><?php echo $jenis_kelamin; ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    Asset::js('jquery.min.js', true);
    Asset::js('popper.min.js', true);
    Asset::js('bootstrap.min.js', true);
    echo Asset::render();
    ?>
</body>

</html>